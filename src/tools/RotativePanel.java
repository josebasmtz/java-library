/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPanel;

/**
 *
 * @author joseluismtz
 */
public class RotativePanel {
    private final JPanel base;
    private JPanel newPanel;
    
    public RotativePanel(JPanel base){
        this.base = base;
        this.base.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                update();
            }
            
        });
    }
    
    private void update() {
        if (newPanel != null) {
            this.newPanel.setSize(new Dimension(this.base.getWidth(), this.base.getHeight()));
            
            this.base.removeAll();
            this.base.add(newPanel);
            this.base.revalidate();
            this.base.repaint();
        }
    }
    
    public void rotate(JPanel p){
        this.newPanel = p;
        update();
    }
}
