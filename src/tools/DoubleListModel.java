/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author joseluismtz
 */
public class DoubleListModel extends AbstractListModel<Object>{
    
    private ArrayList<Double> list;
    private int order;
    public static int ASCENDENT = 3721, DESCENDET = 3722;
    

    public DoubleListModel() {
        this.list = new ArrayList<>();
        this.order = ASCENDENT;
    }

    public void setList(ArrayList<Double> list) {
        this.list.clear();
        for (Double d : list) {
            this.list.add(d);
        }
        this.fireIntervalAdded(this, 0, this.list.size());
    }
    
    public void addDouble(double d){
        this.list.add(d);
        this.fireIntervalAdded(this, 0, list.size());
    }
    
    public void removeDouble(int index){
        if (this.order == ASCENDENT) {
           this.list.remove(index);
        } else {
            this.list.remove((list.size() - 1) - index);
        }
        this.fireIntervalRemoved(this, 0, list.size());
    }

    public void setOrder(int order) throws IllegalArgumentException{
        if (order == ASCENDENT || order == DESCENDET) {
            this.order = order;
        } else {
            throw new IllegalArgumentException("the argument is invalid");
        }
    }
    
    public ArrayList<Double> getList(){
        ArrayList<Double> list = new ArrayList<>();
        for (Double d : this.list) {
            list.add(d);
        }
        return list;
    }
    
    public void removeAll(){
        this.list.clear();
        this.fireIntervalRemoved(this, 0, list.size());
    }
    
    @Override
    public int getSize() {
        return this.list.size();
    }

    @Override
    public Object getElementAt(int index) {
        if (order == ASCENDENT) {
            return this.list.get(index);
        } else {
            return this.list.get((list.size() - 1)- index);
        }
    }
    
}
