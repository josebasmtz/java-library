/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

/**
 *
 * @author joseluismtz
 */
public interface RestrictedInterface {
    
    public void addCharacters(String sequence);
    public void removeCharacters(String sequence);
    public void setLimit(int limit);
    public void setLowerCase();
    public void setUpperCase();
    public void setIgnoreCase();
    public void setAcceptAlphabets(boolean accept);
    public void setAcceptNumbers(boolean accept);
    public void setAcceptSymbols(boolean accept);
    public void setAcceptSpace(boolean accept);
    public void setOnlyAlphabets();
    public void setOnlyNumbers();
    
}
