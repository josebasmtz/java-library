/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.util.ArrayList;

/**
 *
 * @author joseluismtz
 */
public class Bubble {
    
    public static int ASCENDENT = 3721, DESCENDENT = 3722;
    
    public static ArrayList<Double> ordenate(ArrayList<Double> init, int orden){
        if (init != null) {
            ArrayList<Double> find = new ArrayList<>();
            for (Double d : init) {
                find.add(d);
            }
            if (orden == ASCENDENT) {
                double a, b;
                for (int i = 0; i < find.size(); i++) {
                    for (int j = 1; j < find.size(); j++) {
                        a = find.get(j - 1);
                        b = find.get(j);
                        if (a > b) {
                            find.add(j + 1, a);
                            find.remove(j-1);
                        }
                    }
                }
            } else if (orden == DESCENDENT) {
                double a, b;
                for (int i = 0; i < find.size(); i++) {
                    for (int j = 1; j < find.size(); j++) {
                        a = find.get(j - 1);
                        b = find.get(j);
                        if (a < b) {
                            find.add(j + 1, a);
                            find.remove(j-1);
                        }
                    }
                }
            } else {
                throw new IllegalArgumentException("The argument 'orden' is invalid");
            }
            return find;
        } else {
            throw new NullPointerException();
        }
    }
    
}
