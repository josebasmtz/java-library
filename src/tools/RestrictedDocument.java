/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.util.ArrayList;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author joseluismtz
 */
public class RestrictedDocument extends PlainDocument{
    private final ArrayList<Character> charactersAccepted;
    private int limit;
    private boolean lowerCase, upperCase;

    public RestrictedDocument() {
        super();
        this.charactersAccepted = new ArrayList<>();
        this.limit = 40;
        this.lowerCase = false;
        this.upperCase = false;
        for (int i = 'A'; i <= 'Z'; i++) {
            this.addCharacter((char)i);
        }
        for (int i = 'a'; i <= 'z'; i++) {
            this.addCharacter((char)i);
        }
        for (char character : "áéíóúñÁÉÍÓÚÑ".toCharArray()) {
            this.addCharacter(character);
        }
    }
    
    private void addCharacter(Character c){
        if (!charactersAccepted.contains(c)) {
            this.charactersAccepted.add(c);
        }
    }
    
    private void removeCharacter(Character c){
        if (charactersAccepted.contains(c)) {
            this.charactersAccepted.remove(c);
        }
    }
    
    public void addCharacters(String sequence){
        for (char character : sequence.toCharArray()) {
            this.addCharacter(character);
        }
    }
    
    public void removeCharacters(String sequence){
        for (char character : sequence.toCharArray()) {
            this.removeCharacter(character);
        }
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
    
    public void setLowerCase(){
        this.lowerCase = true;
        this.upperCase = false;
    }
    
    public void setUpperCase(){
        this.upperCase = true;
        this.lowerCase = false;
    }
    
    public void setIgnoreCase(){
        this.lowerCase = false;
        this.upperCase = false;
    }
    
    public void setAcceptAlphabets(boolean accept){
        if (accept) {
            for (int i = 'A'; i <= 'Z'; i++) {
                this.addCharacter((char)i);
            }
            for (int i = 'a'; i <= 'z'; i++) {
                this.addCharacter((char)i);
            }
            for (char character : "áÁéÉíÍóÓúÚñÑ".toCharArray()) {
                this.addCharacter(character);
            }
        } else {
            for (int i = 'A'; i <= 'Z'; i++) {
                this.removeCharacter((char)i);
            }
            for (int i = 'a'; i <= 'z'; i++) {
                this.removeCharacter((char)i);
            }
            for (char character : "áÁéÉíÍóÓúÚñÑ".toCharArray()) {
                this.removeCharacter(character);
            }
        }
    }
    
    public void setAcceptNumbers(boolean accept){
        if (accept) {
            for (int i = '0'; i <= '9'; i++) {
                this.addCharacter((char)i);
            }
        } else {
            for (int i = '0'; i <= '9'; i++) {
                this.removeCharacter((char)i);
            }
        }
    }
    
    public void setAcceptSymbols(boolean accept){
        if (accept) {
            for (char character : "|°¬!\"#$%&/()='?\\¿¡¨´+*~{[^}]`,;.:-_<>".toCharArray()) {
                this.addCharacter(character);
            }
        } else {
            for (char character : "|°¬!\"#$%&/()='?\\¿¡¨´+*~{[^}]`,;.:-_<>".toCharArray()) {
                this.removeCharacter(character);
            }
        }
    }
    
    public void setAcceptSpace(boolean accept){
        if (accept) {
            this.addCharacter(' ');
        } else {
            this.removeCharacter(' ');
        }
    }
    
    public void setOnlyAlphabets(){
        this.charactersAccepted.clear();
        this.setAcceptAlphabets(true);
    }
    
    public void setOnlyNumbers(){
        this.charactersAccepted.clear();
        this.setAcceptNumbers(true);
    }
    
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        String newStr = "";
        for (Character character : str.toCharArray()) {
            if (this.getLength() + newStr.length() < this.limit) {
                if (this.charactersAccepted.contains(character)) {
                    newStr += character;
                }
            } else {
                break;
            }
        }
        if (lowerCase) {
            newStr = newStr.toLowerCase();
        } else if (upperCase) {
            newStr = newStr.toUpperCase();
        }
        super.insertString(offs, newStr, a);
    }
    
    
}
