/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JTextField;
import tools.RestrictedDocument;
import tools.RestrictedInterface;

/**
 *
 * @author joseluismtz
 */
public class RestrictedTextField extends JTextField implements RestrictedInterface{
    private final RestrictedDocument document;

    public RestrictedTextField() {
        super();
        this.document = new RestrictedDocument();
        this.document.setLimit(20);
        this.setDocument(document);
    }

    @Override
    public void addCharacters(String sequence) {
        document.addCharacters(sequence);
    }

    @Override
    public void removeCharacters(String sequence) {
        document.removeCharacters(sequence);
    }

    @Override
    public void setLimit(int limit) {
        document.setLimit(limit);
    }

    @Override
    public void setLowerCase() {
        document.setLowerCase();
    }

    @Override
    public void setUpperCase() {
        document.setUpperCase();
    }

    @Override
    public void setIgnoreCase() {
        document.setIgnoreCase();
    }

    @Override
    public void setAcceptAlphabets(boolean accept) {
        document.setAcceptAlphabets(accept);
    }

    @Override
    public void setAcceptNumbers(boolean accept) {
        document.setAcceptNumbers(accept);
    }

    @Override
    public void setAcceptSymbols(boolean accept) {
        document.setAcceptSymbols(accept);
    }

    @Override
    public void setAcceptSpace(boolean accept) {
        document.setAcceptSpace(accept);
    }

    @Override
    public void setOnlyAlphabets() {
        document.setOnlyAlphabets();
    }

    @Override
    public void setOnlyNumbers() {
        document.setOnlyNumbers();
    }
    
}
